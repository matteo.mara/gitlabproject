package org.example;

import static org.example.Utils.method4;

public class Main {
  public static void main(String[] args) {
    method1();

    method2();

    method3();

    method4();
  }

  private static void method3() {
    System.out.println("Hello world!");
    System.out.println("Hello world!");
    System.out.println("Hello world!");
    System.out.println("I am going to add this block of code twyce!");
    System.out.println("GoodBye!");
  }

  private static void method2() {
    System.out.println("Hello world!");
    System.out.println("Hello world!");
    System.out.println("I am going to add this block of code twyce!");
    System.out.println("GoodBye!");
    System.out.println("GoodBye!");
  }

  private static void method1() {
    System.out.println("Hello world!");
    System.out.println("Hello world!");
    System.out.println("I am going to add this block of code twyce!");
    System.out.println("GoodBye!");
    System.out.println("GoodBye!");
  }
}
